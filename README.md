# RTP-Audio Conversion
How to run (with file capture as input) :
1. pip install -r requirements.txt -> install required dependency<br>
2. python main.py --file [file capture] -> create raw audio file<br>
   example:<br>
   python main.py --file test_180s.pcap<br>