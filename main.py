import argparse, os, pyshark, threading, subprocess, re, time, concurrent.futures, glob, queue

def map_rtp_payload(cap):
    client_server_pkt = dict()
    count = 0
    for pkt in cap:
        if hasattr(pkt, 'rtp'):
            if hasattr(pkt.rtp, 'payload'):
                ip_src = pkt.ip.src
                ip_dst = pkt.ip.dst
                src_dst_set = set({ip_src, ip_dst})
                if src_dst_set not in client_server_pkt:
                    client_server_pkt[src_dst_set] = []
                client_server_pkt[src_dst_set].append(pkt.rtp.payload.split(":"))
                count += 1
                if count == 100:
                    break
    return client_server_pkt

def save_rtp_payload(client_server_pkt, folder_name):
    for src_dst in client_server_pkt:
        involved_ip = [ip for ip in src_dst]
        dir_dest = os.getcwd() + '/' + folder_name + '/' + involved_ip[1] + '-' + involved_ip[2] + '.raw'
        raw_audio = open(dir_dest, 'wb')
        for rtp_pkt in client_server_pkt[src_dst]:
            pkt = " ".join(rtp_pkt)
            audio = bytearray.fromhex(pkt)
            raw_audio.write(audio)

def convert_raw_to_wav(folder_dir):
    file_dirs = glob.glob(folder_dir + '/*.raw')
    for file_dir in file_dirs:
        cmd = 'sox -t raw -r 8000 -v 4 -c 1 -b 8 -e unsigned ' +  file_dir + ' ' + re.sub('.raw', '.wav', file_dir)
        subprocess.run(cmd.split(' '))

def process_file(file_name):
    file_dir = os.getcwd() + '/' + file_name
    client_server_pkt = dict()
    cap = pyshark.FileCapture(file_dir, display_filter='rtp')

    clien_server_pkt = map_rtp_payload(cap)

    folder_name = re.sub('.pcap', '', file_dir.split('/')[-1])
    subprocess.run(['mkdir', '-p' ,folder_name])

    save_rtp_payload(client_server_pkt, folder_name)

    folder_dir = os.getcwd() + '/' + folder_name
    convert_raw_to_wav(folder_dir)

parser = argparse.ArgumentParser()
parser.add_argument('--file', nargs='+')

args = parser.parse_args()
with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        executor.map(process_file, args.file)